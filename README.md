# SmogApp

## Abount

It is a simple application with which you can check the air quality in your city.
All you need to do is add your city to the list of subscribed cities.

## Installation guide

Install all dependencies:
```
bundle
```

Copy database.yml
```
cp config/config/database.yml.example config/database.yml
```

Run all pending migrations:
```
bundle exec rake db:migrate
```

Last thing which you need to do, is run application:
```
bundle exec rails s
```