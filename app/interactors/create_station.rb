# frozen_string_literal: true

class CreateStation
  include Interactor

  def call
    station_data = PjpApi::Station.find(context.public_id)
    context.fail! unless station_data

    station = Station.new(
      name:				station_data.name,
      public_id:	station_data.id
    )

    context.station = station
    context.fail! unless station.save
  end
end
