# frozen_string_literal: true

class StationsController < ApplicationController
  before_action :set_station, only: %i[show destroy]

  def index
    @stations = Station.order(created_at: :asc)
  end

  def new
    @stations = PjpApi::Station.all
  end

  def create
    result = CreateStation.call(public_id: station_public_id)

    if result.success?
      redirect_to stations_path, notice: 'The station has been saved'
    else
      render :new
    end
  end

  def show; end

  def destroy
    @station.destroy
    redirect_to stations_path, notice: 'The station has been removed'
  end

  private

  def set_station
    @station = Station.find(params[:id])
  end

  def station_public_id
    params['station_public_id']
  end
end
