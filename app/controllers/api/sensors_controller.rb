# frozen_string_literal: true

module Api
  class SensorsController < ApplicationController
    before_action :set_station

    def index
      @sensors = PjpApi::Sensor.find_all_by_station(@station.public_id)
    end

    private

    def set_station
      @station = Station.find(params[:station_id])
    end
  end
end
