class Application.Models.Sensor extends Backbone.Model

  getLabels: ->
    _.pluck(@get('data'), 'date')

  getData: ->
    _.pluck(@get('data'), 'value')
