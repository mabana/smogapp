#= require ./tree
#= require_directory ./models
#= require_directory ./collections
#= require_directory ./views

class App extends Marionette.Application
  region: '#app-container',

  onStart: ->
    @sensors = Application.Collections.Sensors.getForStation(@getStationId())
    @sensors.fetch().done =>
      @renderSensorsView()

  renderSensorsView: ->
    @view = new Application.Views.SensorsView(collection: @sensors)
    @showView(@view)

  getStationId: ->
    window.location.pathname.split('/').slice(-1)[0]

document.addEventListener 'turbolinks:load', ->
  container = $('#app-container')

  if container.length
    window.app = new App
    app.start()


