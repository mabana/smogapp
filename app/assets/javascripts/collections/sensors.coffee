class Application.Collections.Sensors extends Backbone.Collection
  model: Application.Models.Sensor

  @getForStation: (id) ->
    collection = new @
    collection.url = "/api/stations/#{id}/sensors"
    collection
