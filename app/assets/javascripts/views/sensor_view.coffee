class Application.Views.SensorView extends Marionette.View
  template: _.template(
    """
      <canvas></canvas>
    """
  )

  ui:
    canvas: 'canvas'

  onRender: ->
    @renderChart()

  renderChart: ->
    ctx = @ui.canvas.get(0).getContext('2d')

    new Chart ctx,
      type: 'line'
      data:
        labels: @model.getLabels()
        datasets: [
          label: @model.get('name')
          data: @model.getData()
        ]
