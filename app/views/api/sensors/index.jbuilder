# frozen_string_literal: true

json.array! @sensors do |sensor|
  json.id   sensor.id
  json.name sensor.param_name

  json.data do
    json.merge! sensor.data
  end
end
