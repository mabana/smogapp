# frozen_string_literal: true

module PjpApi
  HOSTNAME = 'api.gios.gov.pl'.freeze

  def self.get(path)
    JSON.parse(
      Net::HTTP.get(HOSTNAME, path)
    )
  end
end
