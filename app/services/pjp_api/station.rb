# frozen_string_literal: true

module PjpApi
  class Station
    PATH = '/pjp-api/rest/station/findAll'.freeze

    attr_reader :id, :name

    def self.all
      PjpApi.get(PATH).map(&method(:new))
    end

    # This API doesn't provide show endpoint
    def self.find(station_id)
      all.find do |station|
        station.id == station_id.to_i
      end
    end

    def initialize(attributes)
      @id   = attributes['id']
      @name = attributes['stationName']
    end

    def sensors
      Sensor.find_all_by_station(id)
    end
  end
end
