# frozen_string_literal: true

module PjpApi
  class Sensor
    PATH = '/pjp-api/rest/station/sensors/'.freeze

    attr_reader :id, :param_name, :param_formula, :param_code

    def self.find_all_by_station(station_id)
      PjpApi.get("#{PATH}#{station_id}").map(&method(:new))
    end

    def initialize(attributes)
      param = attributes['param']

      @id             = attributes['id']
      @param_name     = param['paramName']
      @param_formula  = param['paramFormula']
      @param_code     = param['paramCode']
    end

    def data
      PjpApi::Data.find_all_by_sensor(@id)
    end
  end
end
