# frozen_string_literal: true

module PjpApi
  class Data
    PATH = '/pjp-api/rest/data/getData/'.freeze

    attr_reader :date, :value

    def self.find_all_by_sensor(sensor_id)
      PjpApi.get("#{PATH}#{sensor_id}")['values'].map(&method(:new))
    end

    def initialize(attributes)
      @date   = attributes['date']
      @value  = attributes['value']
    end
  end
end
