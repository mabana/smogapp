# frozen_string_literal: true

class Station < ApplicationRecord
  validates :name, :public_id, presence: true
end
