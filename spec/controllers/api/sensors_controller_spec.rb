# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::SensorsController, type: :controller do
  describe 'GET #index' do
    let(:station) { build(:station) }

    before do
      pjpapi_data = instance_double(
        PjpApi::Data,
        date:   '2017-03-28 11:00:00',
        value:  103.21
      )

      pjpapi_sensor = instance_double(
        PjpApi::Sensor,
        id: 2,
        param_name:     'CO2',
        param_formula:  'co2',
        param_code:     'co2',
        data:           [pjpapi_data]
      )

      expect(Station).to receive(:find).with('1').and_return(station)
      expect(PjpApi::Sensor).to receive(:find_all_by_station).with(station.public_id)
                                                             .and_return([pjpapi_sensor])
      get :index, params: { station_id: 1 }
    end

    it { is_expected.to respond_with :success }
  end
end
