require 'rails_helper'

RSpec.describe StationsController, type: :controller do
  describe 'GET #index' do
    let(:station) { build_list(:station, 2) }

    before do
      expect(Station).to receive(:order).with(created_at: :asc)
                                        .and_return(station)

      get :index
    end

    it { is_expected.to respond_with :success }
  end

  describe 'GET #new' do
    before do
      station = instance_double(PjpApi::Station, id: 1, name: 'Cracow')
      expect(PjpApi::Station).to receive(:all).and_return([station])
      get :new
    end

    it { is_expected.to respond_with :success }
  end

  describe 'POST #create' do
    context 'when the parameters are correct' do
      before do
        ctx = Module.new do
          def self.success?
            true
          end
        end

        expect(CreateStation).to receive(:call).with(public_id: '2').and_return(ctx)
        post :create, params: { station_public_id: 2 }
      end

      it { is_expected.to redirect_to(stations_path) }
    end

    context 'when the parameters are not correct' do
      before do
        ctx = Module.new do
          def self.success?
            false
          end
        end

        expect(CreateStation).to receive(:call).with(public_id: '2').and_return(ctx)
        post :create, params: { station_public_id: 2 }
      end

      it { is_expected.to respond_with :success }
    end
  end

  describe 'GET #show' do
    let(:station) { build(:station) }

    before do
      expect(Station).to receive(:find).with('2').and_return(station)
      get :show, params: { id: 2 }
    end

    it { is_expected.to respond_with :success }
  end

  describe 'DELETE #destroy' do
    let(:station) { build(:station) }

    before do
      expect(Station).to receive(:find).with('2').and_return(station)
      expect(station).to receive(:destroy).and_return(true)
      delete :destroy, params: { id: 2 }
    end

    it { is_expected.to redirect_to(stations_path) }
  end
end
