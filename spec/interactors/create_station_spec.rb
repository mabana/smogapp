# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateStation do
  describe '#call' do
    context 'when station is not exist in public api' do
      before do
        expect(PjpApi::Station).to receive(:find).with(3).and_return(nil)
      end

      it 'return fail' do
        expect(
          described_class.call(public_id: 3).success?
        ).to be(false)
      end
    end

    context "when station can't be saved" do
      before do
        station = instance_double(PjpApi::Station, id: 3, name: '')
        expect(PjpApi::Station).to receive(:find).with(3).and_return(station)
        expect_any_instance_of(Station).to receive(:save).and_return(false)
      end

      it 'return fail' do
        expect(
          described_class.call(public_id: 3).success?
        ).to be(false)
      end
    end

    context 'when data is correct' do
      before do
        station = instance_double(PjpApi::Station, id: 3, name: '')
        expect(PjpApi::Station).to receive(:find).with(3).and_return(station)
        expect_any_instance_of(Station).to receive(:save).and_return(true)
      end

      it 'save new station in database' do
        expect(
          described_class.call(public_id: 3).success?
        ).to be(true)
      end
    end
  end
end
