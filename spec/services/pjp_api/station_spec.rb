# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PjpApi::Station do
  let(:first_station_attributes) do
    { 'id' => 1, 'stationName' => 'Cracow' }
  end

  let(:last_station_attributes) do
    { 'id' => 2, 'stationName' => 'Warsaw' }
  end

  describe '.all' do
    it 'return array of station' do
      example_response = [
        first_station_attributes,
        last_station_attributes
      ]

      expect(PjpApi).to receive(:get).with(
        '/pjp-api/rest/station/findAll'
      ).and_return(example_response)

      result = described_class.all
      expect(result.count).to be(2)

      first_station = result.first
      expect(first_station.id).to be(1)
      expect(first_station.name).to eq('Cracow')

      last_station = result.last
      expect(last_station.id).to be(2)
      expect(last_station.name).to eq('Warsaw')
    end
  end

  describe '.find' do
    it 'return station which has specific id' do
      expect(described_class).to receive(:all).and_return(
        [
          described_class.new(first_station_attributes),
          described_class.new(last_station_attributes)
        ]
      )

      result = described_class.find(1)
      expect(result.id).to be(1)
      expect(result.name).to eq('Cracow')
    end
  end

  describe '#sensors' do
    it 'call Sensor.find_all_by_station' do
      station = described_class.new(first_station_attributes)
      expect(PjpApi::Sensor).to receive(:find_all_by_station).with(station.id)
      station.sensors
    end
  end
end
