# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PjpApi::Data do
  describe '.find_all_by_sensor' do
    it 'return data from sensor' do
      example_response = {
        'key' => 'PM10',
        'values' => [
          { 'date' => '2017-03-28 11:00:00', 'value' => 30.3018 }
        ]
      }

      expect(
        PjpApi
      ).to receive(
        :get
      ).with(
        '/pjp-api/rest/data/getData/2'
      ).and_return(
        example_response
      )

      result = described_class.find_all_by_sensor(2)
      expect(result.count).to be(1)

      data = result.first
      expect(data.date).to eq('2017-03-28 11:00:00')
      expect(data.value).to be(30.3018)
    end
  end
end
