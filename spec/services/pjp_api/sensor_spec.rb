# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PjpApi::Sensor do
  let(:example_sensor_attributes) do
    {
      'id' => 2,
      'param' => {
        'paramName' => 'carbon dioxide',
        'paramFormula' => 'CO2',
        'paramCode' => 'co2'
      }
    }
  end

  describe '.find_all_by_station' do
    it 'return sensors which belongs to station' do
      expect(
        PjpApi
      ).to receive(
        :get
      ).with(
        '/pjp-api/rest/station/sensors/2'
      ).and_return(
        [example_sensor_attributes]
      )

      result = described_class.find_all_by_station(2)
      expect(result.count).to be(1)

      sensor = result.first
      expect(sensor.id).to be(2)
      expect(sensor.param_name).to eq('carbon dioxide')
      expect(sensor.param_formula).to eq('CO2')
      expect(sensor.param_code).to eq('co2')
    end
  end

  describe '#data' do
    it 'call PjpApi::Data.find_all_by_station' do
      expect(PjpApi::Data).to receive(:find_all_by_sensor).with(2)

      sensor = described_class.new(example_sensor_attributes)
      sensor.data
    end
  end
end
