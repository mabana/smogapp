# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PjpApi do
  describe '.get' do
    it 'return parsed response' do
      template = { 'a' => 1 }

      expect(Net::HTTP).to receive(:get).with('api.gios.gov.pl', '/a')
                                        .and_return(template.to_json)

      expect(
        described_class.get('/a')
      ).to eq(template)
    end
  end
end
