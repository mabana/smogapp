# frozen_string_literal: true

FactoryBot.define do
  factory :station do
    name      { FFaker::Address.city }
    public_id 1
  end
end
