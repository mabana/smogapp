# frozen_string_literal: true

Rails.application.routes.draw do
  root 'stations#index'

  resources :stations

  namespace :api, constraints: { format: 'json' } do
    resources :stations, only: [] do
      resources :sensors, only: :index
    end
  end
end
